default: build

BUILDDIR = _build

.PHONY: configure build install


$(BUILDDIR):
	meson $(BUILDDIR) || (rm -rf $(BUILDDIR) ; exit 1)


configure: | $(BUILDDIR)

build: configure
	ninja -C $(BUILDDIR)

install: build
	ninja -C $(BUILDDIR) install
