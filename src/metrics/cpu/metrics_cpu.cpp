#include "metrics_cpu.h"


#include <iostream>

// Initialisation
bool metrics_initialize() {
    std::cout << "Hello cpu metrics !" << std::endl;
    return true;
}

// Get the percent value for all times
size_t all_history_values(double* buffer, size_t buffer_len) {
    size_t values_count = std::min(buffer_len, size_t(100));


    for (size_t i = 0; i < values_count; i++)
    {
        buffer[i] = i;
    }

    return values_count;
}
