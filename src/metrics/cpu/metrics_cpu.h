#pragma once

#include "stdlib.h"
#include "stdbool.h"

#ifdef __cplusplus
extern "C" {
#endif


extern char* metrics_name;

// Initialisation
bool metrics_initialize();

// Get the percent value for all times
size_t all_history_values(double* buffer, size_t buffer_len);


#ifdef __cplusplus
}
#endif
