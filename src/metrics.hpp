#pragma once

#include <string>
#include <vector>

void set_modules_path();

class Module {
public:
    Module(std::string const& file_name);

    virtual ~Module();

    // Checks that a module was correctly found
    operator bool() const { return m_found; }

    // FIXME: useful ?
    // Ensures that a module will never be unloaded.
    // void make_resident();

    // Gets a symbol pointer from the module.
    bool get_symbol(std::string const& symbol_name, void*& symbol) const;

    // Get the name of the module
    std::string get_name() const { return m_file_name; };


    Module(Module&& other) noexcept;
    Module& operator=(Module&& other) noexcept;
    Module& operator=(const Module&) = delete;

private:
    std::string const m_file_name;
    bool m_found;

    void* m_handle;
};




class Metrics : Module {
public:
    Metrics(std::string const& lib_path)
    : Module(lib_path) {

    }
    ~Metrics() = default;



private:
    bool find_library();
    bool find_functions();


    std::vector<std::string> m_lib_functions;

};
