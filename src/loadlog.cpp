#include <iostream>


#include "metrics.hpp"

#include "metrics/cpu/metrics_cpu.h"

typedef bool (*init_function_type)(void);



int main(int argc, char const *argv[])
{

    Module cpu_module("src/metrics/cpu/libcpu.dll");

    if (!cpu_module) {
        return 1;
    }

    init_function_type init_function;

    if (!cpu_module.get_symbol("metrics_initialize", (void*&)init_function)) {
        return 1;
    }
    init_function();

    return 0;
}
