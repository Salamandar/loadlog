#include "metrics.hpp"
#include "config.h"

#include <iostream>

#ifdef HAVE_DL
#   include <dlfcn.h>
#elif defined(PLATFORM_WIN64)
#   include <windows.h>
#else
#   error "DL library is available, unless on Windows."
#endif


std::vector<Module> All_Modules;

Module::Module(std::string const& file_name)
: m_file_name(file_name) {

    #ifdef HAVE_DL

        m_handle = dlopen(file_name.c_str(), RTLD_NOW | RTLD_LOCAL);

        if (!m_handle) {
            std::cerr << dlerror() << std::endl;
            return;
        }

    #elif defined(PLATFORM_WIN64)

        m_handle = LoadLibrary(file_name.c_str());

        if (!m_handle) {
            std::cerr << "Could not find library " + file_name << std::endl;
            return;
        }

    #endif
    m_found = true;
}

Module::~Module() {
    #ifdef HAVE_DL
        dlclose(m_handle);
    #endif
}

bool Module::get_symbol(std::string const& symbol_name, void*& symbol) const {
    #ifdef HAVE_DL
        symbol = dlsym(m_handle, symbol_name.c_str());
    #elif defined(PLATFORM_WIN64)
        FARPROC initializer = GetProcAddress(HMODULE(m_handle), symbol_name.c_str());
        symbol = (void*) initializer;
    #endif

    if (symbol == 0) {
        std::cerr << "Could not find function '" + symbol_name + "' in library " << std::endl;
    }

    return (symbol != 0);
}
